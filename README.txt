-- SUMMARY --

This module, on installation looks for the file 
Australian_Post_Codes_Lat_Lon.csv
and if found imports it into the database and exposes the items as entities.

-- REQUIREMENTS --

Entity API module

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.